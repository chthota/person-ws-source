package example.endpoint;

import org.example.person.schema.GetAllPersonsResponse;
import org.example.person.schema.Person;

public class GetAllPersonsEndpoint extends AbstractPersonEndpoint {
  @Override
  protected Object invokeInternal(Object request) throws Exception {
    GetAllPersonsResponse getAllPersonsResponse = new GetAllPersonsResponse();
    for (Person person : personService.getAllPersons()) {
      getAllPersonsResponse.getPerson().add(person);
    }
    return getAllPersonsResponse;
  }
}
