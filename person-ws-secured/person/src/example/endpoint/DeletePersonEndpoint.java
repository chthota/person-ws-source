package example.endpoint;

import org.example.person.schema.DeletePersonRequest;

public class DeletePersonEndpoint extends AbstractPersonEndpoint {
  @Override
  protected Object invokeInternal(Object request) throws Exception {
    DeletePersonRequest deletePersonRequest = (DeletePersonRequest) request;
    personService.deletePerson(deletePersonRequest.getId());
    return null;
  }
}
