package example.endpoint;

import org.example.person.schema.UpdatePersonRequest;

public class UpdatePersonEndpoint extends AbstractPersonEndpoint {
  @Override
  protected Object invokeInternal(Object request) throws Exception {
    UpdatePersonRequest updatePersonRequest = (UpdatePersonRequest) request;
    personService.updatePerson(updatePersonRequest.getPerson());
    return null;
  }
}
