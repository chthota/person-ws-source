package example.client;

import org.example.person.schema.Person;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.security.wss4j.Wss4jSecurityInterceptor;

public class Main {
  public static void main(String[] args) {
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    PersonClient client = (PersonClient) ctx.getBean("client");
    
    Wss4jSecurityInterceptor interceptor = (Wss4jSecurityInterceptor) ctx.getBean("wsSecurityInterceptor");
   
    System.out.println("Bert is authorized");
    setUsernameToken(interceptor, "Bert", "Ernie");    
    doGetPerson(client, 1);
    
    System.out.println("Bert got wrong password");
    setUsernameToken(interceptor, "Bert", "Big Bird");
    doGetPerson(client, 1);
    
    System.out.println("Mickey is also authorized");
    setUsernameToken(interceptor, "Mickey", "Mouse");
    doGetPerson(client, 2);
    
    System.out.println("Batman is not authorized");
    setUsernameToken(interceptor, "Batman", "Robin");
    doGetPerson(client, 2);
  }

  private static void doGetPerson(PersonClient client, int id) {
    try {
      Person person = null;
      System.out.println("Get person with id=" + id + "...");
      person = client.getPerson(id);
      System.out.println("\tPerson Response " + makeString(person));
    } catch (SoapFaultClientException se) {
      System.out.println("\t" + se.getMessage());
    }
  }
  
  private static String makeString(Person p) {
    return "[id=" + p.getId() + ", firstName=" + p.getFirstName() + ", lastName=" + p.getLastName() + "]";
  }
  
  private static void setUsernameToken(Wss4jSecurityInterceptor interceptor, String user, String pass) {
    interceptor.setSecurementUsername(user);
    interceptor.setSecurementPassword(pass);
  }
}
