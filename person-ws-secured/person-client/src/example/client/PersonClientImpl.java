package example.client;

import java.util.List;

import org.example.person.schema.AddPersonRequest;
import org.example.person.schema.DeletePersonRequest;
import org.example.person.schema.GetAllPersonsRequest;
import org.example.person.schema.GetAllPersonsResponse;
import org.example.person.schema.GetPersonRequest;
import org.example.person.schema.GetPersonResponse;
import org.example.person.schema.Person;
import org.example.person.schema.UpdatePersonRequest;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class PersonClientImpl extends WebServiceGatewaySupport implements PersonClient {

  @Override
  public void addPerson(Person person) {
    AddPersonRequest request = new AddPersonRequest();
    request.setPerson(person);
    getWebServiceTemplate().marshalSendAndReceive(request);
  }

  @Override
  public void deletePerson(Integer id) {
    DeletePersonRequest request = new DeletePersonRequest();
    request.setId(id);
    getWebServiceTemplate().marshalSendAndReceive(request);
  }

  @Override
  public List<Person> getAllPersons() {
    GetAllPersonsRequest request = new GetAllPersonsRequest();
    GetAllPersonsResponse response = (GetAllPersonsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    return response.getPerson();
  }

  @Override
  public Person getPerson(Integer id) {
    GetPersonRequest request = new GetPersonRequest();
    request.setId(id);
    GetPersonResponse response = (GetPersonResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    return response.getPerson();
  }

  @Override
  public void updatePerson(Person person) {
    UpdatePersonRequest request = new UpdatePersonRequest();
    request.setPerson(person);
    getWebServiceTemplate().marshalSendAndReceive(request);
  }
}
