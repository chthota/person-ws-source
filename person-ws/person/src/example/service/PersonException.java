package example.service;

public class PersonException extends Exception {
  public PersonException(String message) {
    super(message);
  }
}
