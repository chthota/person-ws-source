package example.endpoint;

import org.springframework.ws.server.endpoint.AbstractMarshallingPayloadEndpoint;

import example.service.PersonService;

public abstract class AbstractPersonEndpoint extends AbstractMarshallingPayloadEndpoint {
  protected PersonService personService;

  public void setPersonService(PersonService personService) {
    this.personService = personService;
  }
  protected abstract Object invokeInternal(Object request) throws Exception;
}
