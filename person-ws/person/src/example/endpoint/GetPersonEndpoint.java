package example.endpoint;

import org.example.person.schema.GetPersonRequest;
import org.example.person.schema.GetPersonResponse;

public class GetPersonEndpoint extends AbstractPersonEndpoint {
  @Override
  protected Object invokeInternal(Object request) throws Exception {
    GetPersonRequest getPersonRequest = (GetPersonRequest) request;
    GetPersonResponse getPersonResponse = new GetPersonResponse();
    getPersonResponse.setPerson(personService.getPerson(getPersonRequest.getId()));
    return getPersonResponse;
  }
}
