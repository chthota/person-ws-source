package example.endpoint;

import org.example.person.schema.AddPersonRequest;

public class AddPersonEndpoint extends AbstractPersonEndpoint {
  @Override
  protected Object invokeInternal(Object request) throws Exception {
    AddPersonRequest addPersonRequest = (AddPersonRequest) request;
    personService.addPerson(addPersonRequest.getPerson());
    return null;
  }
}
